import OpenGL.GL as gl
import OpenGL.GLU as glu
import numpy as np
import pygame
from Cilindro import *
from Dodecaedro import *
from Prisma import *
from Cubo import *
from Icosaedro import *
from Piramide import *
from Octaedro import *
from Esfera import *
from Cono import *
from Cactus import *
from pygame.locals import *
import sys, os, traceback
#Para la implementacion de luces
import LUCES
from Textura import *
from PIL import Image as Image



if sys.platform in ["win32","win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"
from math import *
#Inicializacion
pygame.display.init()
pygame.font.init()
screen_size = [1000,600]
multisample = 0
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
pygame.display.set_caption("Figuras primitivas")
if multisample:
    pygame.display.gl_set_attribute(GL_MULTISAMPLEBUFFERS,1)
    pygame.display.gl_set_attribute(GL_MULTISAMPLESAMPLES,multisample)

pygame.display.set_mode(screen_size,OPENGL|DOUBLEBUF)

gl.glHint(gl.GL_PERSPECTIVE_CORRECTION_HINT,gl.GL_NICEST)

gl.glEnable(gl.GL_DEPTH_TEST)

camera_rot = [30.0,20.0]      #The spherical coordinates' angles (degrees).
camera_radius = 3.0           #The sphere's radius
camera_center = [0.0,0.0,0.0]


##Llamamos a las diferentes clases
# Creamos los objetos
ci = Cilindro(1, 1, gl, np)
do = Dodecaedro(gl)
pri = Prisma(gl)
cu = Cubo(gl)
ico = Icosaedro(gl)
pi = Piramide(gl)
oc = Octaedro(gl)
es = Esfera(gl, glu)
co = Cono(gl, np)
cact=Cactus(gl,glu,np)

global varX
varX=0

global varY
varY=5

global varZ
varZ=0

LUCES.IniciarIluminacion(varX,varY,varZ)
tex=Textura(gl,glu,pygame)
def get_input():
    global camera_rot, camera_radius


    keys_pressed = pygame.key.get_pressed()
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_position = pygame.mouse.get_pos()

    mouse_rel = pygame.mouse.get_rel()

    for event in pygame.event.get():


        if   event.type == QUIT: return False

        elif event.type == KEYDOWN:

            if   event.key == K_ESCAPE: return False

        elif event.type == MOUSEBUTTONDOWN:

            if   event.button == 4: camera_radius *= 0.9

            elif event.button == 5: camera_radius /= 0.9




    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True

#Variable global para el teclado
global varN
varN=10
global textID

global cont
cont=0

def draw():
    global varN, varX, varY, varZ, cont
    global textID

    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)


    gl.glViewport(0, 0, screen_size[0], screen_size[1])

    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()

    glu.gluPerspective(45, float(screen_size[0]) / float(screen_size[1]), 0.1, 100.0)

    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glLoadIdentity()

    camera_pos = [
        camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
    ]

    glu.gluLookAt(
        camera_pos[0], camera_pos[1], camera_pos[2],
        camera_center[0], camera_center[1], camera_center[2],
        0, 1, 0
    )
    #Mueve el objeto moviendo el mouse
    #tank_distance = pygame.mouse.get_pos()[1] / 50.0
    #gl.glTranslatef(-tank_distance, -1.5, 0)

    #Dibujar lineas centrales
    Lineas()
    # Dibuja la luz
    Luz()
    #Llamamos a los metodos de las clases, de acuerdo al numero que ingresamos por teclado, enviando por los parametros
    #las diferentes rasnformaciones y datos para la creacion de las figuras
    Teclado()
    LUCES.IniciarIluminacion(varX,varY,varZ)

    #imagen="piedra.jpg"
    #tex.crearTextura(imagen)
    #ci.CrearCilindro(1, 1, 1, 2,1,1,1,0,0,0,0,0,0,1)


    imagen = "./Texturas/a.jpg"
    tex.crearTextura(imagen)
    cact.crearCactus(1,1,1,1,1.3,1,0,0,0,0,0,0)
    #cact.crearCactus(1, 1, 1, 0.5, 0.7, 0.5, -5, -2, 0, 0, 0, 0)

    pygame.display.flip()

#Funcion del teclado
def Luz():
    global varX, varY, varZ
    gl.glPushMatrix()
    gl.glTranslate(varX,varY,varZ)
    sphere = glu.gluNewQuadric();
    gl.glColor3f(1, 1, 1)
    glu.gluQuadricDrawStyle(sphere, glu.GLU_FILL);
    glu.gluSphere(sphere, 0.3, 20, 20);
    gl.glPopMatrix()

def Teclado():
    global  varN, varX, varY, varZ

    teclado = pygame.key.get_pressed()

    if teclado[K_1]:
        #Cambia el nombre de la ventana
        pygame.display.set_caption('Cilindro')
        varN = 1


    if teclado[K_2]:
        pygame.display.set_caption('Dodecaedro')
        varN = 2

    if teclado[K_3]:
        pygame.display.set_caption('Prisma')
        varN = 3

    if teclado[K_4]:
        pygame.display.set_caption('Cubo')
        varN = 4

    if teclado[K_5]:
        pygame.display.set_caption('Icosaedro')
        varN = 5

    if teclado[K_6]:
        pygame.display.set_caption('Piramide')
        varN = 6

    if teclado[K_7]:
        pygame.display.set_caption('Octaedro')
        varN = 7

    if teclado[K_8]:
        pygame.display.set_caption('Esfera')
        varN = 8

    if teclado[K_9]:
        pygame.display.set_caption('Cono')
        varN = 9

    if teclado[K_RIGHT]:
        varX=varX+0.05

    if teclado[K_LEFT]:
        varX=varX-0.05

    if teclado[K_UP]:
        varY=varY+0.05

    if teclado[K_DOWN]:
        varY=varY-0.05

    if teclado[K_z]:
        varZ=varZ+0.05

    if teclado[K_a]:
        varZ=varZ-0.05


def Lineas():
    gl.glBegin(gl.GL_LINES)
    # Change the color to red.  All subsequent geometry we draw will be red.
    gl.glColor3f(1, 0, 0)
    # Make two vertices, thereby drawing a (red) line.
    gl.glVertex(0, 0, 0)
    gl.glVertex3f(5, 0, 0)
    # Change the color to green.  All subsequent geometry we draw will be green.
    gl.glColor3f(0, 1, 0)
    # Make two vertices, thereby drawing a (green) line.
    gl.glVertex(0, 0, 0)
    gl.glVertex3f(0, 5, 0)
    # Change the color to blue.  All subsequent geometry we draw will be blue.
    gl.glColor3f(0, 0, 1)
    # Make two vertices, thereby drawing a (blue) line.
    gl.glVertex(0, 0, 0)
    gl.glVertex3f(0, 0, 5)
    gl.glEnd()

def main():
    #global  texture_id
    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        draw()

        clock.tick(60)
        tex.borrarTex()
        print("textura", tex.text_id)

    pygame.quit()






if __name__ == "__main__":
    try:
        main()


    except:
        traceback.print_exc()
        pygame.quit()
        input()
