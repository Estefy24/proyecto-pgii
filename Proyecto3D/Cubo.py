#Autor: Joana Estefanía Nicolalde
#Fecha: 26/06/2020
#Clase Cubo
#Descripcion: Una clase que mantiene todos los componentes necesarios para la creacion del cubo y sus diferentes transformaciones
#Es llamada por la clase main.
#Ultima modificacion:3/07/2020 Integrando Textura y luces

import math
class Cubo:
    #Inicializador
    def __init__(self,gl):
        self.gl = gl

    #Funcion para crear un cubo
    def crearCubo(self,r,g,b,ex,ey,ez,tx,ty,tz,rx,ry,rz,v):
        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)
        #Definimos vertices
        self.gl.glBegin(self.gl.GL_QUADS)
        self.gl.glColor3f(r, g, b)
        self.gl.glTexCoord2f(0, 0)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        self.gl.glTexCoord2f(v, 0)
        #self.gl.glColor3f(b*0.8, g*0.8, r*0.8)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        self.gl.glTexCoord2f(v, v)
        #self.gl.glColor3f(r*0.9, g*0.9, b*0.9)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        #self.gl.glColor3f(b*0.8, g*0.8, r*0.8)
        self.gl.glTexCoord2f(0, v)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        self.gl.glColor3f(r*0.85, g*0.85, b*0.85)
        self.gl.glTexCoord2f(0, 0)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        #self.gl.glColor3f(b*0.6, g*0.6, r*0.6)
        self.gl.glTexCoord2f(v, 0)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        #self.gl.glColor3f(r * 0.7, g * 0.7, b * 0.7)
        self.gl.glTexCoord2f(v, v)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        #self.gl.glColor3f(b*0.6, g*0.6, r*0.6)
        self.gl.glTexCoord2f(0, v)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        self.gl.glColor3f(r*0.75, g*0.75, b*0.75)
        self.gl.glTexCoord2f(0, 0)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        #self.gl.glColor3f(0, 0, b)
        self.gl.glTexCoord2f(v, 0)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        #self.gl.glColor3f(0, g, 0)
        self.gl.glTexCoord2f(v, v)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        #self.gl.glColor3f(0, 0, b)
        self.gl.glTexCoord2f(0, v)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        self.gl.glEnd()

        #cara abajo
        self.gl.glBegin(self.gl.GL_QUADS)
        self.gl.glColor3f(r*0.6, g*0.6, b*0.6)
        self.gl.glTexCoord2f(0, 0)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        #self.gl.glColor3f(0, 0, b)
        self.gl.glTexCoord2f(v, 0)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        #self.gl.glColor3f(0, g, 0)
        self.gl.glTexCoord2f(v, v)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        #self.gl.glColor3f(0, 0, b)
        self.gl.glTexCoord2f(0, v)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        self.gl.glColor3f(r*0.75, g*0.75, b*0.75)
        self.gl.glTexCoord2f(0, 0)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        #self.gl.glColor3f(0, 0, b)
        self.gl.glTexCoord2f(v, 0)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        #self.gl.glColor3f(0, g, 0)
        self.gl.glTexCoord2f(v, v)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        #self.gl.glColor3f(0, 0, b)
        self.gl.glTexCoord2f(0, v)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        self.gl.glColor3f(r*0.85, g*0.85, b*0.85)
        self.gl.glTexCoord2f(0, 0)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        #self.gl.glColor3f(0, 0, b)
        self.gl.glTexCoord2f(v, 0)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        #self.gl.glColor3f(0, g, 0)
        self.gl.glTexCoord2f(v, v)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        #self.gl.glColor3f(0, 0, b)
        self.gl.glTexCoord2f(0, v)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        self.gl.glEnd()

        self.gl.glPopMatrix()

    def crearCuboWireFrame(self,r,g,b,s,ex,ey,ez,tx,ty,tz,rx,ry,rz):

        self.gl.glPushMatrix()

        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s)


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        self.gl.glEnd()

        self.gl.glPopMatrix()