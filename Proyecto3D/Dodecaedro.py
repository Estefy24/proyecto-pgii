#Autor: Joana Estefanía Nicolalde
#Fecha: 26/06/2020
#Clase Dodecaedro
#Descripcion: Una clase que mantiene todos los componentes necesarios para la creacion del dodecaedro y sus diferentes transformaciones
#Es llamada por la clase main.
class Dodecaedro:
    #Inicializador
    def __init__(self,gl):
        self.gl = gl



    def CrearDodecaedro(self,r,g,b,ex,ey,ez,tx,ty,tz,rx,ry,rz):


        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glPushMatrix()

        self.gl.glScalef(0.02, 0.02, 0.02)
        self.gl.glTranslate(0, -35, 0)

        self.gl.glBegin(self.gl.GL_POLYGON)
        self.gl.glColor3f(r, g, b)
        #self.gl.glTexCoord2f(13.499026, 26.998053)
        self.gl.glVertex3f(13.499026, 26.998053, 41.545731)  # 1
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(35.340908, 43.683765, 25.676674)  # 2
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(21.841883, 70.681816, 15.869058)  # 5
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
        #self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3

        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_POLYGON)
        self.gl.glColor3f(r*0.85, g*0.85, b*0.85)
        self.gl.glVertex3f(21.841883, 70.681816, 15.869058)  # 5
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(35.340908, 43.683765, 25.676674)  # 2
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(43.683765, 26.998053, 0.000000)  # 6
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
        #self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_POLYGON)
        self.gl.glColor3f(r*0.75, g*0.75, b*0.75)
        self.gl.glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
        #self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_POLYGON)
        self.gl.glColor3f(r*0.65, g*0.65, b*0.65)
        self.gl.glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
        #self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_POLYGON)
        self.gl.glColor3f(r*0.85, g*0.85, b*0.85)
        self.gl.glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
        #self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_POLYGON)
        self.gl.glColor3f(r*0.75, g*0.75, b*0.75)
        self.gl.glVertex3f(13.499026, 26.998053, 41.545731)  # 1
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17
        #self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(8.342857, 0.000000, 25.676674)  # 16
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_POLYGON)
        self.gl.glColor3f(r * 0.65, g * 0.65, b * 0.65)
        self.gl.glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
        #self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17

        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_POLYGON)
        self.gl.glColor3f(r*0.75, g*0.75, b*0.75)
        self.gl.glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
        self.gl.glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
        #self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_POLYGON)
        self.gl.glColor3f(r*0.85, g*0.85, b*0.85)
        self.gl.glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
        self.gl.glVertex3f(21.841883, 70.681816, 15.869058)  # 5
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
        #self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_POLYGON)
        self.gl.glColor3f(r*0.75, g*0.75, b*0.75)
        self.gl.glVertex3f(43.683765, 26.998053, 0.000000)  # 6
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
        #self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_POLYGON)
        self.gl.glColor3f(r*0.65, g*0.65, b*0.65)
        self.gl.glVertex3f(13.499026, 26.998053, 41.545731)  # 1
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(8.342857, 0.000000, 25.676674)  # 16
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(43.683765, 26.998053, 0.000000)  # 6
        #self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(35.340908, 43.683765, 25.676674)  # 2
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_POLYGON)
        self.gl.glColor3f(r*0.75, g*0.75, b*0.75)
        self.gl.glVertex3f(8.342857, 0.000000, 25.676674)  # 16
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17
        #self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
        #self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
        #self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
        self.gl.glEnd()

        self.gl.glPopMatrix()
        self.gl.glPopMatrix()

    def CrearDodecaedroWireFrame(self, r, g, b,s,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        #Color y tamaño de linea para el wireframe
        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s)

        self.gl.glPushMatrix()
        #Para las diferentes transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glPushMatrix()
        self.gl.glScalef(0.02, 0.02, 0.02)
        self.gl.glTranslate(0, -35, 0)
        #VERTICES
        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(13.499026, 26.998053, 41.545731)  # 1
        self.gl.glVertex3f(35.340908, 43.683765, 25.676674)  # 2
        self.gl.glVertex3f(21.841883, 70.681816, 15.869058)  # 5
        self.gl.glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
        self.gl.glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3

        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(21.841883, 70.681816, 15.869058)  # 5
        self.gl.glVertex3f(35.340908, 43.683765, 25.676674)  # 2
        self.gl.glVertex3f(43.683765, 26.998053, 0.000000)  # 6
        self.gl.glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
        self.gl.glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
        self.gl.glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
        self.gl.glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
        self.gl.glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
        self.gl.glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
        self.gl.glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
        self.gl.glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
        self.gl.glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
        self.gl.glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
        self.gl.glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
        self.gl.glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
        self.gl.glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
        self.gl.glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(13.499026, 26.998053, 41.545731)  # 1
        self.gl.glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3
        self.gl.glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
        self.gl.glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17
        self.gl.glVertex3f(8.342857, 0.000000, 25.676674)  # 16
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
        self.gl.glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
        self.gl.glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
        self.gl.glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
        self.gl.glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17

        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
        self.gl.glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
        self.gl.glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
        self.gl.glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
        self.gl.glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
        self.gl.glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
        self.gl.glVertex3f(21.841883, 70.681816, 15.869058)  # 5
        self.gl.glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
        self.gl.glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
        self.gl.glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(43.683765, 26.998053, 0.000000)  # 6
        self.gl.glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
        self.gl.glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
        self.gl.glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
        self.gl.glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(13.499026, 26.998053, 41.545731)  # 1
        self.gl.glVertex3f(8.342857, 0.000000, 25.676674)  # 16
        self.gl.glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
        self.gl.glVertex3f(43.683765, 26.998053, 0.000000)  # 6
        self.gl.glVertex3f(35.340908, 43.683765, 25.676674)  # 2
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(8.342857, 0.000000, 25.676674)  # 16
        self.gl.glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17
        self.gl.glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
        self.gl.glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
        self.gl.glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
        self.gl.glEnd()

        self.gl.glPopMatrix()
        self.gl.glPopMatrix()