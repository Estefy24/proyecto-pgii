#Autor: Joana Estefanía Nicolalde
#Fecha: 26/06/2020
#Clase Cilindro
#Descripcion: Una clase que mantiene todos los componentes necesarios para la creacion del cilindro y sus diferentes transformaciones
#Es llamada por la clase main.
#Ultima modificacion:3/07/2020 Integrando Textura y luces

import math
class Cilindro:
    #Inicializador
    def __init__(self,alto, radio,gl,np):
        self.alto=alto
        self.radio=radio
        self.gl = gl
        self.np = np

#Metodo CrearCilindro
    def CrearCilindro(self,r,g,b,tapas,ex,ey,ez,tx,ty,tz,rx,ry,rz,v):

        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex,ey,ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glBegin(self.gl.GL_QUAD_STRIP)
        for i in self.np.arange(0, 7, 0.1):
            auxX = math.sin(i) * self.radio
            auxZ = math.cos(i) * self.radio
            self.gl.glColor3f(r, g, b)
            self.gl.glTexCoord2f(auxX*v, 0)
            self.gl.glVertex(auxX, 0, auxZ)
            self.gl.glColor3f(r*0.8, g*0.8, b*0.8)
            self.gl.glTexCoord2f(auxX*v, self.alto*v)
            self.gl.glVertex(auxX, self.alto, auxZ)
            self.gl.glColor3f(r*0.6, g*0.6, b*0.6)

        self.gl.glEnd()

        #Crear Tapas, si se requiere: 0, 1 o 2
        if(tapas==1):

            self.CrearTapasCilindro(self.alto,r,g,b,v)
        else:
            if(tapas==2):
                self.CrearTapasCilindro(self.alto, r, g, b,v)
                self.CrearTapasCilindro(0, r, g, b,v)


        self.gl.glPopMatrix()

    #Metodo para crear las tapas
    def CrearTapasCilindro(self,q,r,g,b,v):
        self.gl.glBegin(self.gl.GL_POLYGON)
        for i in self.np.arange(0, 7, 0.1):
            auxX = math.sin(i) * self.radio
            auxZ = math.cos(i) * self.radio
            self.gl.glTexCoord2f(auxX*v, auxZ*v)
            self.gl.glVertex(auxX, q, auxZ)
            self.gl.glColor3f(r*0.9, g*0.9, b*0.9)
        self.gl.glEnd()




#Para crear el cilindro en wireframe
    def CrearCilindroWireframe(self, r, g, b,s,ex,ey,ez,tx,ty,tz,rx,ry,rz):


        self.gl.glPushMatrix()

        self.gl.glScalef(ex,ey,ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx,1, 0, 0)
        self.gl.glRotate(ry,0,1,0)
        self.gl.glRotate(rz,0,0,1)

        self.gl.glColor3f(r,g,b)
        self.gl.glLineWidth(s)

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        for i in self.np.arange(0, 7, 0.1):
            auxX = math.sin(i) * self.radio
            auxZ = math.cos(i) * self.radio
            self.gl.glColor3f(r, g, b)
            self.gl.glVertex(auxX, 0, auxZ)
            self.gl.glColor3f(b, r - 0.5, r)
            self.gl.glVertex(auxX, self.alto, auxZ)
            self.gl.glColor3f(b, r, r)

        self.gl.glEnd()
        #Si es que tiene tapas
        self.CrearTapasCilindroWireframe(self.alto, r, g, b, s)
        self.CrearTapasCilindroWireframe(0, r, g, b, s)

        self.gl.glPopMatrix()

    # Metodo para crear las tapas en wireframe

    def CrearTapasCilindroWireframe(self, v, r, g, b, s):

        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s)

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        for i in self.np.arange(0, 7, 0.1):
            auxX = math.sin(i) * self.radio
            auxZ = math.cos(i) * self.radio
            self.gl.glVertex(auxX, v, auxZ)
        self.gl.glEnd()