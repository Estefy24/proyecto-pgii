#Autor:Joana Estefanía Nicolalde
#Clase para implementar una textura
#Descripcion: Una clase para crear una textura con todos los parametros.Se la llama del Main

from ctypes import c_long
from pyglet.gl import glGetIntegerv, GL_MAX_TEXTURE_SIZE
class Textura:
    global text_id
    text_id=0
   #Inicializador
    def __init__(self, gl,glu,pygame):
        self.gl = gl
        self.glu = glu
        self.pygame=pygame
        self.text_id=text_id
    #Funcion de creación de textura con parametro de imagen
    def crearTextura(self, imagen):
        # Load the textures
        #global text_id
        texture_surface = self.pygame.image.load(imagen)
        # Retrieve the texture data
        texture_data = self.pygame.image.tostring(texture_surface, 'RGB', True)

        # Generate a texture id
        self.text_id = self.gl.glGenTextures(1)
        # Tell OpenGL we will be using this texture id for texture operations
        self.gl.glBindTexture(self.gl.GL_TEXTURE_2D, self.text_id)

        # Tell OpenGL how to scale images
        #self.gl.glTexParameteri(self.gl.GL_TEXTURE_2D, self.gl.GL_TEXTURE_MAG_FILTER, self.gl.GL_LINEAR)
        #self.gl.glTexParameteri(self.gl.GL_TEXTURE_2D, self.gl.GL_TEXTURE_MIN_FILTER, self.gl.GL_LINEAR)

        #self.gl.glTexParameteri(self.gl.GL_TEXTURE_2D, self.gl.GL_TEXTURE_MAG_FILTER, self.gl.GL_NEAREST)
        #self.gl.glTexParameteri(self.gl.GL_TEXTURE_2D, self.gl.GL_TEXTURE_MIN_FILTER, self.gl.GL_NEAREST_MIPMAP_NEAREST)

        self.gl.glTexParameteri(self.gl.GL_TEXTURE_2D, self.gl.GL_TEXTURE_WRAP_S, self.gl.GL_MIRRORED_REPEAT)
        self.gl.glTexParameteri(self.gl.GL_TEXTURE_2D, self.gl.GL_TEXTURE_WRAP_T, self.gl.GL_MIRRORED_REPEAT)


        # Tell OpenGL that data is aligned to byte boundries
        self.gl.glPixelStorei(self.gl.GL_UNPACK_ALIGNMENT, 1)

        # Get the dimensions of the image
        width, height = texture_surface.get_rect().size

        # gluBuild2DMipmaps
        self.glu.gluBuild2DMipmaps(self.gl.GL_TEXTURE_2D,
                          3,
                          width,
                          height,
                          self.gl.GL_RGB,
                          self.gl.GL_UNSIGNED_BYTE,
                          texture_data)


        i = c_long()
        glGetIntegerv(GL_MAX_TEXTURE_SIZE, i)
        #print(i)  # output: c_long(1024) (or higher)
        #print("Max",self.gl.glGetIntegerv(self.gl.GL_MAX_TEXTURE_SIZE))

        return self.text_id

    def borrarTex(self):
        self.gl.glDeleteTextures(text_id)
