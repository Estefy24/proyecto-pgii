#Autor: Joana Estefanía Nicolalde
#Fecha: 26/06/2020
#Clase Piramide
#Descripcion: Una clase que mantiene todos los componentes necesarios para la creacion de la piramide y sus diferentes transformaciones
#Es llamada por la clase main.
class Piramide:
    #Inicializador
   def __init__(self, gl):
        self.gl = gl

    #Creacion de la piramide con sus diferentes vertices
   def crearPiramide(self,r,g,b,lado,ex,ey,ez,tx,ty,tz,rx,ry,rz,v,textura):
       self.gl.glPushMatrix()
        #Transformaciones
       self.gl.glScalef(ex, ey, ez)
       self.gl.glTranslate(tx, ty, tz)
       self.gl.glRotate(rx, 1, 0, 0)
       self.gl.glRotate(ry, 0, 1, 0)
       self.gl.glRotate(rz, 0, 0, 1)

       # Frente
       self.gl.glBegin(self.gl.GL_TRIANGLES)
       self.gl.glColor3f(r,g,b)
       if(textura):
        self.gl.glTexCoord2f(0, 0)

       self.gl.glVertex(0, 0, 0)
       #self.gl.glColor3f(0, g, 0)
       if (textura):
           self.gl.glTexCoord2f((lado / 2)*v, (lado / 2)*v)

       self.gl.glVertex(lado / 2, lado, lado / 2)
       #self.gl.glColor3f(0, 0, b)
       if (textura):
           self.gl.glTexCoord2f(lado*v, 0)
       self.gl.glVertex(lado, 0, 0)
       self.gl.glEnd()

       # LateralDerecho

       self.gl.glBegin(self.gl.GL_TRIANGLES)
       self.gl.glColor3f(r*0.85,g*0.85,b*0.85)
       if (textura):
        self.gl.glTexCoord2f(0, 0)
       self.gl.glVertex(lado, 0, 0)
       #self.gl.glColor3f(0, b, 0)
       if (textura):
           self.gl.glTexCoord2f((lado / 2)*v, (lado / 2)*v)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       #self.gl.glColor3f(0, 0, g)
       if (textura):
        self.gl.glTexCoord2f(lado*v, 0)
       self.gl.glVertex(lado, 0, lado)
       self.gl.glEnd()

       # LateralDerecho

       self.gl.glBegin(self.gl.GL_TRIANGLES)
       self.gl.glColor3f(r*0.75,g*0.75,b*0.75)
       if (textura):
        self.gl.glTexCoord2f(0, 0)
       self.gl.glVertex(0, 0, 0)
       #self.gl.glColor3f(0, b, 0)
       if (textura):
        self.gl.glTexCoord2f((lado / 2)*v, (lado / 2)*v)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       #self.gl.glColor3f(0, 0, g)
       if (textura):
        self.gl.glTexCoord2f(lado*v, 0)
       self.gl.glVertex(0, 0, lado)
       self.gl.glEnd()

       # LateralDerecho
       self.gl.glBegin(self.gl.GL_TRIANGLES)
       self.gl.glColor3f(r*0.95,g*0.95,b*0.95)
       if (textura):
        self.gl.glTexCoord2f(0, 0)
       self.gl.glVertex(0, 0, lado)
       #self.gl.glColor3f(0, b, 0)
       if (textura):
        self.gl.glTexCoord2f((lado / 2)*v, (lado / 2)*v)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       #self.gl.glColor3f(0, 0, g)
       if (textura):
        self.gl.glTexCoord2f(lado*v, 0)
       self.gl.glVertex(lado, 0, lado)
       self.gl.glEnd()

        #cara de abajo
       self.gl.glBegin(self.gl.GL_QUADS)
       self.gl.glColor3f(r*0.6,g*0.6,b*0.6)
       if (textura):
        self.gl.glTexCoord2f(0, 0)
       self.gl.glVertex(0, 0, 0)
       #self.gl.glColor3f(0, b, 0)
       if (textura):
        self.gl.glTexCoord2f(lado*v, 0)
       self.gl.glVertex(lado, 0, 0)
       #self.gl.glColor3f(0, 0, g)
       if (textura):
        self.gl.glTexCoord2f(lado*v, lado*v)
       self.gl.glVertex(lado, 0, lado)
       #self.gl.glColor3f(r, 0, 0)
       if (textura):
        self.gl.glTexCoord2f(0, lado*v)
       self.gl.glVertex(0, 0, lado)
       self.gl.glEnd()

       self.gl.glPopMatrix()

   def crearPiramideWireFrame(self, r, g, b, s,lado,ex,ey,ez,tx,ty,tz,rx,ry,rz):
       self.gl.glPushMatrix()

       self.gl.glScalef(ex, ey, ez)
       self.gl.glTranslate(tx, ty, tz)
       self.gl.glRotate(rx, 1, 0, 0)
       self.gl.glRotate(ry, 0, 1, 0)
       self.gl.glRotate(rz, 0, 0, 1)


       self.gl.glColor3f(r, g, b)
       self.gl.glLineWidth(s)

       # Frente
       self.gl.glBegin(self.gl.GL_LINE_LOOP)
       self.gl.glVertex(0, 0, 0)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       self.gl.glVertex(lado, 0, 0)
       self.gl.glEnd()

       # LateralDerecho

       self.gl.glBegin(self.gl.GL_LINE_LOOP)
       self.gl.glVertex(lado, 0, 0)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       self.gl.glVertex(lado, 0, lado)
       self.gl.glEnd()

       # LateralDerecho

       self.gl.glBegin(self.gl.GL_LINE_LOOP)
       self.gl.glVertex(0, 0, 0)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       self.gl.glVertex(0, 0, lado)
       self.gl.glEnd()

       # LateralDerecho
       self.gl.glBegin(self.gl.GL_LINE_LOOP)
       self.gl.glVertex(0, 0, lado)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       self.gl.glVertex(lado, 0, lado)
       self.gl.glEnd()

       self.gl.glBegin(self.gl.GL_LINE_LOOP)
       self.gl.glVertex(0, 0, 0)
       self.gl.glVertex(lado, 0, 0)
       self.gl.glVertex(lado, 0, lado)
       self.gl.glVertex(0, 0, lado)
       self.gl.glEnd()

       self.gl.glPopMatrix()