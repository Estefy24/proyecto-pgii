#Autor: Joana Estefanía Nicolalde
#Fecha: 26/06/2020
#Clase Octaedro
#Descripcion: Una clase que mantiene todos los componentes necesarios para la creacion del octaedro y sus diferentes transformaciones
#Es llamada por la clase main.
#Importamos una clase que vamos a usar
from Piramide import *
class Octaedro:
    #Inicializador
   def __init__(self, gl):
        self.gl = gl

    #Clase para crear un Octaedro
   def crearOctaedro(self,r,g,b,lado,ex,ey,ez,tx,ty,tz,rx,ry,rz):
       self.gl.glPushMatrix()
        #Transformaciones
       self.gl.glScalef(ex, ey, ez)
       self.gl.glTranslate(tx, ty, tz)
       self.gl.glRotate(rx, 1, 0, 0)
       self.gl.glRotate(ry, 0, 1, 0)
       self.gl.glRotate(rz, 0, 0, 1)

       medioSup = Piramide(self.gl)
       medioInf = Piramide(self.gl)
       medioSup.crearPiramide(r, g, b, lado,1,1,1,0,0,0,0,0,0,1)

       self.gl.glPushMatrix()
       self.gl.glRotate(180,1,0,0)
       self.gl.glRotate(90, 0, 1, 0)
       medioInf.crearPiramide(r, g, b, lado,1,1,1,0,0,0,0,0,0,1)
       self.gl.glPopMatrix()

       self.gl.glPopMatrix()


    #Crear un octaedro wireframe
   def crearOctaedroWireFrame(self,r,g,b,s,lado,ex,ey,ez,tx,ty,tz,rx,ry,rz):
       self.gl.glPushMatrix()

       self.gl.glScalef(ex, ey, ez)
       self.gl.glTranslate(tx, ty, tz)
       self.gl.glRotate(rx, 1, 0, 0)
       self.gl.glRotate(ry, 0, 1, 0)
       self.gl.glRotate(rz, 0, 0, 1)

       medioSup = Piramide(self.gl)
       medioInf = Piramide(self.gl)
       medioSup.crearPiramideWireFrame(r, g, b, s, lado,1,1,1,0,0,0,0,0,0)

       self.gl.glPushMatrix()
       self.gl.glRotate(180,1,0,0)
       self.gl.glRotate(90, 0, 1, 0)
       medioInf.crearPiramideWireFrame(r, g, b,s, lado,1,1,1,0,0,0,0,0,0)
       self.gl.glPopMatrix()

       self.gl.glPopMatrix()
