from Esfera import *
from Cono import *

class Cactus:
    #Inicializador
    def __init__(self,gl,glu,np):
        self.gl = gl
        self.glu=glu
        self.np=np

    #Funcion para crear un cubo
    def crearCactus(self,r,g,b,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        self.gl.glPushMatrix()
        # Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        #creo el objeto
        es=Esfera(self.gl,self.glu)
        co=Cono(self.gl,self.np)

        #Cuerpo
        es.crearEsfera(1, 20, 20, 1, 1, 1, 1, 3, 1, 0, 0, 0, 0, 0, 0)

        # brazo uno
        es.crearEsfera(1, 20, 20, 1, 1, 1, 0.5, 1.5, 0.5, 3, 0, 0, 0, 0, -60)
        es.crearEsfera(1, 20, 20, 1, 1, 1, 0.45, 1.2, 0.45, 4, 0.6, 0, 0, 0, -20)

        # brazo dos

        es.crearEsfera(1, 20, 20, 1, 1, 1, 0.5, 1.5, 0.5, -1.3, 0.4, 0, 0, 0, 45)
        es.crearEsfera(1, 20, 20, 1, 1, 1, 0.4, 1, 0.4, -4, 1.3, 0, 0, 0, 5)

        #espinas
        espinas(self.gl,co,0,0,0,1,1,1,0,0,0)

        espinas(self.gl,co,0,-1,0,1,1,1,10,0,0)
        espinas(self.gl, co, 0, -1, 0, 1, 1, 1, -10, 0, 0)


        espinas(self.gl, co, 0, 0, 0, 1, 1, 1, 0, 180, 0)
        espinas(self.gl, co, 0, -1, 0, 1, 1, 1, -10, 180, 0)
        espinas(self.gl, co, 0, -1, 0, 1, 1, 1, 0, 180, 0)

        espinas(self.gl, co, 0, -1, 0, 1, 1, 1, 0, 90, 0)
        espinas(self.gl, co, 0.5, -0.5, 0.1, 1, 1, 1, 10, 90, 0)
        espinas(self.gl, co, 0.2, 0, 0, 1, 1, 1, 10, 90, 0)
        espinas(self.gl, co, -0.5, -0.5, 0.1, 1, 1, 1, 0, 90, 0)

        #parte de atras

        espinas(self.gl, co, 0, -0.1,0.1, 1, 1, 1, 0, 270, 0)

        espinas(self.gl, co, 0.5, -0.3, -0.2, 1, 1, 1, 0, 270, 0)
        espinas(self.gl, co, -0.5, -0.5, -0.2, 1, 1, 1, 0, 270, 0)

        #espinas brazos
        espinas(self.gl, co, 1.45, -0.3, 0.1, 0.7, 0.5, 0.7, 0, 0, -25)
        espinas(self.gl, co, 1.45, -0.3, 0.2, 0.7, 0.5, 0.7, 0, 0,-20)

        self.gl.glPopMatrix()


def espinas(gl,co,tx,ty,tz,ex,ey,ez,rx,ry,rz):

    gl.glPushMatrix()

    gl.glTranslate(tx,ty,tz)
    gl.glScalef(ex,ey,ez)
    gl.glRotate(rx, 1, 0, 0)
    gl.glRotate(ry, 0, 1, 0)
    gl.glRotate(rz, 0, 0, 1)

    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 2, 2, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 1.8, -3, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 1.5, 2, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 2, 7, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 1.8, 12, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 1.5, 17, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 1.2, 22, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 0.5, 27, 0, 0, 0, -90, 0, False)

    gl.glPopMatrix()