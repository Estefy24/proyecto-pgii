#Autor: Joana Estefanía Nicolalde
#Fecha: 26/06/2020
#Clase Prisma
#Descripcion: Una clase que mantiene todos los componentes necesarios para la creacion del prisma y sus diferentes transformaciones
#Es llamada por la clase main.
class Prisma:
    #Inicializador
    def __init__(self, gl):
        self.gl = gl

    #Para crear el prisma wireframe con sus diferentes vertices
    def crearPrismaWireFrame(self,r,g,b,s,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s) #Linea tamaño

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glTexCoord2f(1, 1)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glEnd()

        self.gl.glPopMatrix()
    #Funcion que crea el prisma con sus vertices
    def crearPrisma(self, r, g, b,ex,ey,ez,tx,ty,tz,rx,ry,rz,v):
        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f( r, g, b)
        self.gl.glTexCoord2f(1*v, -1*v)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        #self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glTexCoord2f(-1*v, 0)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        #self.gl.glColor3f(0.3 + g, 0.5, 0.5)
        self.gl.glTexCoord2f(1*v, 1*v)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glEnd()


        #cara de abajo
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.7,g*0.7,b*0.7)
        self.gl.glTexCoord2f(-1*v, 0)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        #self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glTexCoord2f(1*v, -1*v)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        #self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glTexCoord2f(1*v, 1*v)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.75, g*0.75, b*0.75)
        self.gl.glTexCoord2f(1*v, 1*v)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        #self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glTexCoord2f(-1*v, -1*v)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        #self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glTexCoord2f(1*v, -1*v)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.85, g*0.85, b*0.85)
        self.gl.glTexCoord2f(1*v, 1*v)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        #self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glTexCoord2f(-1*v, -1*v)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        #self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glTexCoord2f(1*v, -1*v)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r * 0.8, g * 0.8, b * 0.8)
        self.gl.glTexCoord2f(1*v, 1*v)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        #self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glTexCoord2f(-1*v, -1*v)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        #self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glTexCoord2f(1*v, -1*v)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r * 0.75, g * 0.75, b * 0.75)
        self.gl.glTexCoord2f(1*v, 1*v)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        #self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glTexCoord2f(-1*v, 1*v)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        #self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glTexCoord2f(-1*v, -1*v)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r * 0.85, g * 0.85, b * 0.85)
        self.gl.glTexCoord2f(1*v, 1*v)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        #self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glTexCoord2f(-1*v, 1*v)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        #self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glTexCoord2f(-1*v, -1*v)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r * 0.8, g * 0.8, b * 0.8)
        self.gl.glTexCoord2f(1*v, 1*v)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        #self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glTexCoord2f(-1*v, 1*v)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        #self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glTexCoord2f(-1*v, -1*v)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glEnd()

        self.gl.glPopMatrix()



