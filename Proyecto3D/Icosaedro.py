#Autor: Joana Estefanía Nicolalde
#Fecha: 26/06/2020
#Clase Icosaedro
#Descripcion: Una clase que mantiene todos los componentes necesarios para la creacion del icosaedro y sus diferentes transformaciones
#Es llamada por la clase main.
class Icosaedro:
    #Inicializador
    def __init__(self, gl):
        self.gl = gl
    #Funcion para crear un icosaedro
    def crearIcosaedro(self,r,g,b,ex,ey,ez,tx,ty,tz,rx,ry,rz):

        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)
        #Definiendo vertices
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(0.000000, -1.000000, 0.000000)
        #self.gl.glColor3f(r, b, b)
        self.gl.glVertex3f(0.723600, -0.447215, 0.525720)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(-0.276385, -0.447215, 0.850640)
        self.gl.glEnd()

        #1,1,0
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.9, g*0.9, b*0.9)
        self.gl.glVertex3f(0.723600, -0.447215, 0.525720)
        #self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(0.000000, -1.000000, 0.000000)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(0.723600, -0.447215, -0.525720)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.8, g*0.8, b*0.8)
        self.gl.glVertex3f(0.000000, -1.000000, 0.000000)
        #self.gl.glColor3f(r, b, b)
        self.gl.glVertex3f(-0.276385, -0.447215, 0.850640)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(-0.894425, -0.447215, 0.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.7, g*0.7, b*0.7)
        self.gl.glVertex3f(0.000000, -1.000000, 0.000000)
        #self.gl.glColor3f(r, b, b)
        self.gl.glVertex3f(-0.894425, -0.447215, 0.000000)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(-0.276385, -0.447215, -0.850640)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.6, g*0.6, b*0.6)
        self.gl.glVertex3f(0.000000, -1.000000, 0.000000)
        #self.gl.glColor3f(r, b, b)
        self.gl.glVertex3f(-0.276385, -0.447215, -0.850640)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(0.723600, -0.447215, -0.525720)
        self.gl.glEnd()

        # fin de la parte 1

        # parte media

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.5, g*0.5, b*0.5)
        self.gl.glVertex3f(0.723600, -0.447215, 0.525720)
        #self.gl.glColor3f(r, b, b)
        self.gl.glVertex3f(0.723600, -0.447215, -0.525720)
        #self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(0.894425, 0.447215, 0.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.4, g*0.4, b*0.4)
        self.gl.glVertex3f(-0.276385, -0.447215, 0.850640)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(0.723600, -0.447215, 0.525720)
        #self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(0.276385, 0.447215, 0.850640)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.3, g*0.3, b*0.3)
        self.gl.glVertex3f(-0.894425, -0.447215, 0.000000)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(-0.276385, -0.447215, 0.850640)
        #self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(-0.723600, 0.447215, 0.525720)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.4, g*0.4, b*0.4)
        self.gl.glVertex3f(-0.276385, -0.447215, -0.850640)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(-0.894425, -0.447215, 0.000000)
        #self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(-0.723600, 0.447215, -0.525720)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.5, g*0.5, b*0.5)
        self.gl.glVertex3f(0.723600, -0.447215, -0.525720)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(-0.276385, -0.447215, -0.850640)
        #self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(0.276385, 0.447215, -0.850640)
        self.gl.glEnd()

        # aqui

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.6, g*0.6, b*0.6)
        self.gl.glVertex3f(0.723600, -0.447215, 0.525720)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(0.894425, 0.447215, 0.000000)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(0.276385, 0.447215, 0.850640)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.7, g*0.7, b*0.7)
        self.gl.glVertex3f(-0.276385, -0.447215, 0.850640)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(0.276385, 0.447215, 0.850640)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(-0.723600, 0.447215, 0.525720)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.8, g*0.8, b*0.8)
        self.gl.glVertex3f(-0.894425, -0.447215, 0.000000)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(-0.723600, 0.447215, 0.525720)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(-0.723600, 0.447215, -0.525720)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.9, g*0.9, b*0.9)
        self.gl.glVertex3f(-0.276385, -0.447215, -0.850640)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(-0.723600, 0.447215, -0.525720)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(0.276385, 0.447215, -0.850640)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(0.723600, -0.447215, -0.525720)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(0.276385, 0.447215, -0.850640)
        #self.gl.glColor3f(r, b, g)
        self.gl.glVertex3f(0.894425, 0.447215, 0.000000)
        self.gl.glEnd()

        # holaaa

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.9, g*0.9, b*0.9)
        self.gl.glVertex3f(0.276385, 0.447215, 0.850640)
        #self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(0.894425, 0.447215, 0.000000)
        #self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(0.000000, 1.000000, 0.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.8, g*0.8, b*0.8)
        self.gl.glVertex3f(-0.723600, 0.447215, 0.525720)
        #self.gl.glColor3f(r, b, b)
        self.gl.glVertex3f(0.276385, 0.447215, 0.850640)
        #self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(0.000000, 1.000000, 0.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.7, g*0.7, b*0.7)
        self.gl.glVertex3f(-0.723600, 0.447215, -0.525720)
        #self.gl.glColor3f(r, b, b)
        self.gl.glVertex3f(-0.723600, 0.447215, 0.525720)
        #self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(0.000000, 1.000000, 0.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.6, g*0.6, b*0.6)
        self.gl.glVertex3f(0.276385, 0.447215, -0.850640)
        #self.gl.glColor3f(r, b, b)
        self.gl.glVertex3f(-0.723600, 0.447215, -0.525720)
        #self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(0.000000, 1.000000, 0.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r*0.5, g*0.5, b*0.5)
        self.gl.glVertex3f(0.894425, 0.447215, 0.000000)
        #self.gl.glColor3f(r, b, b)
        self.gl.glVertex3f(0.276385, 0.447215, -0.850640)
        #self.gl.glColor3f(r, g, b)
        self.gl.glVertex3f(0.000000, 1.000000, 0.000000)
        self.gl.glEnd()

        self.gl.glPopMatrix()


    def crearIcosaedroWireframe(self,r,g,b,s,ex,ey,ez,tx,ty,tz,rx,ry,rz):


        self.gl.glPushMatrix()

        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s)

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(0.000000, -1.000000, 0.000000)
        self.gl.glVertex3f(0.723600, -0.447215, 0.525720)
        self.gl.glVertex3f(-0.276385, -0.447215, 0.850640)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(0.723600, -0.447215, 0.525720)
        self.gl.glVertex3f(0.000000, -1.000000, 0.000000)
        self.gl.glVertex3f(0.723600, -0.447215, -0.525720)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(0.000000, -1.000000, 0.000000)
        self.gl.glVertex3f(-0.276385, -0.447215, 0.850640)
        self.gl.glVertex3f(-0.894425, -0.447215, 0.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(0.000000, -1.000000, 0.000000)
        self.gl.glVertex3f(-0.894425, -0.447215, 0.000000)
        self.gl.glVertex3f(-0.276385, -0.447215, -0.850640)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(0.000000, -1.000000, 0.000000)
        self.gl.glVertex3f(-0.276385, -0.447215, -0.850640)
        self.gl.glVertex3f(0.723600, -0.447215, -0.525720)
        self.gl.glEnd()

        # fin de la parte 1

        # parte media

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(0.723600, -0.447215, 0.525720)
        self.gl.glVertex3f(0.723600, -0.447215, -0.525720)
        self.gl.glVertex3f(0.894425, 0.447215, 0.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-0.276385, -0.447215, 0.850640)
        self.gl.glVertex3f(0.723600, -0.447215, 0.525720)
        self.gl.glVertex3f(0.276385, 0.447215, 0.850640)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-0.894425, -0.447215, 0.000000)
        self.gl.glVertex3f(-0.276385, -0.447215, 0.850640)
        self.gl.glVertex3f(-0.723600, 0.447215, 0.525720)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-0.276385, -0.447215, -0.850640)
        self.gl.glVertex3f(-0.894425, -0.447215, 0.000000)
        self.gl.glVertex3f(-0.723600, 0.447215, -0.525720)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(0.723600, -0.447215, -0.525720)
        self.gl.glVertex3f(-0.276385, -0.447215, -0.850640)
        self.gl.glVertex3f(0.276385, 0.447215, -0.850640)
        self.gl.glEnd()

        # aqui

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(0.723600, -0.447215, 0.525720)
        self.gl.glVertex3f(0.894425, 0.447215, 0.000000)
        self.gl.glVertex3f(0.276385, 0.447215, 0.850640)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-0.276385, -0.447215, 0.850640)
        self.gl.glVertex3f(0.276385, 0.447215, 0.850640)
        self.gl.glVertex3f(-0.723600, 0.447215, 0.525720)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-0.894425, -0.447215, 0.000000)
        self.gl.glVertex3f(-0.723600, 0.447215, 0.525720)
        self.gl.glVertex3f(-0.723600, 0.447215, -0.525720)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-0.276385, -0.447215, -0.850640)
        self.gl.glVertex3f(-0.723600, 0.447215, -0.525720)
        self.gl.glVertex3f(0.276385, 0.447215, -0.850640)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(0.723600, -0.447215, -0.525720)
        self.gl.glVertex3f(0.276385, 0.447215, -0.850640)
        self.gl.glVertex3f(0.894425, 0.447215, 0.000000)
        self.gl.glEnd()

        # holaaa

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(0.276385, 0.447215, 0.850640)
        self.gl.glVertex3f(0.894425, 0.447215, 0.000000)
        self.gl.glVertex3f(0.000000, 1.000000, 0.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-0.723600, 0.447215, 0.525720)
        self.gl.glVertex3f(0.276385, 0.447215, 0.850640)
        self.gl.glVertex3f(0.000000, 1.000000, 0.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-0.723600, 0.447215, -0.525720)
        self.gl.glVertex3f(-0.723600, 0.447215, 0.525720)
        self.gl.glVertex3f(0.000000, 1.000000, 0.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(0.276385, 0.447215, -0.850640)
        self.gl.glVertex3f(-0.723600, 0.447215, -0.525720)
        self.gl.glVertex3f(0.000000, 1.000000, 0.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(0.894425, 0.447215, 0.000000)
        self.gl.glVertex3f(0.276385, 0.447215, -0.850640)
        self.gl.glVertex3f(0.000000, 1.000000, 0.000000)
        self.gl.glEnd()

        self.gl.glPopMatrix()