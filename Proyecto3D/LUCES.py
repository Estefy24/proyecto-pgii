#Clase para la implementacion de panatalla y luces
#Fecha:01/07/2020

from OpenGL.GL import *
from OpenGL.GLU import *

def IniciarPantalla(screen):
    width = screen[0]
    height = screen[1]
    if height == 0:
        height = 1
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, 1.0 * width / height, 1.0, 10000.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def IniciarIluminacion(x,y,z):
    glEnable(GL_BLEND)  # masking functions
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)

    glShadeModel(GL_SMOOTH)
    glEnable(GL_TEXTURE_2D)
    # Colores de fondo
    #glClearColor(0.27, 0.45, 0.9, 0.0) # Celeste
    #glClearColor(0.5, 0.5, 0.5, 0.0) # Gris
    glClearColor(0.0, 0.0, 0.0, 0.0) # Negro
    glClearDepth(1.0)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_ALPHA_TEST)
    glDepthFunc(GL_LEQUAL)
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

    glAlphaFunc(GL_NOTEQUAL, 0.0)

    # glFogfv(GL_FOG_DENSITY, .01)
    # glFogfv(GL_FOG_COLOR, (1,1,1,0.5))
    # glEnable(GL_FOG)

    # Parámetros de color de luz
    LightPosition = [x, y, z, 1]
    LightAmbient = [0.5, 0.5, 0.5, 1.0]
    LightDiffuse = [1.0, 1.0, 1.0, 1.0]
    LightIntensity = [50.0, 50.0, 50.0, 10.0]



    # Tipos de luces
    glLightfv(GL_LIGHT0, GL_POSITION, LightPosition)
    glLightfv(GL_LIGHT0, GL_SPECULAR, LightIntensity)
    # glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient)
    # glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse)


    glMaterialfv(GL_FRONT, GL_DIFFUSE, LightDiffuse)
    glMaterialfv(GL_FRONT, GL_AMBIENT, LightAmbient)
    glMaterial(GL_FRONT, GL_SHININESS, 100.0)

    glLightfv(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.1)
    glLightfv(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.05)

    glEnable(GL_LIGHT0)
    glEnable(GL_LIGHTING)
    glEnable(GL_COLOR_MATERIAL)

