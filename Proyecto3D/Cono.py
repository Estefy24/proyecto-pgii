#Autor: Joana Estefanía Nicolalde
#Fecha: 26/06/2020
#Clase Cono
#Descripcion: Una clase que mantiene todos los componentes necesarios para la creacion del cono y sus diferentes transformaciones
#Es llamada por la clase main.
#Ultima modificacion:3/07/2020 Integrando Textura y luces
import math
class Cono:
    def __init__(self, gl,np):
        self.gl = gl
        self.np=np

    def crearCono(self,r,g,b,radio,alto,ex,ey,ez,tx,ty,tz,rx,ry,rz,v,textura):

        self.gl.glPushMatrix()

        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glBegin(self.gl.GL_POLYGON)
        for i in self.np.arange(0, 7, 0.1):
            auxX = math.sin(i) * radio
            auxZ = math.cos(i) * radio
            self.gl.glColor3f(r*0.8, g*0.8, b*0.8)
            if (textura):
                self.gl.glTexCoord2f(auxX*v, auxZ*v) #para colocar la textura
            self.gl.glVertex(auxX, 0, auxZ)

            #self.gl.glVertex(auxX, 0, auxZ)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUAD_STRIP)

        for i in self.np.arange(0, 6.309, 0.1):
            auxX = math.sin(i) * radio
            auxZ = math.cos(i) * radio
            self.gl.glColor3f(r*0.9, g*0.9, b*0.7)
            if (textura):
                self.gl.glTexCoord2f(auxX*v, 0)
            self.gl.glVertex(auxX, 0, auxZ)
            self.gl.glColor3f(r*0.5, g*0.5, b*0.5)
            if (textura):
                self.gl.glTexCoord2f(0,alto*v)
            self.gl.glVertex(0, alto, 0)
        self.gl.glEnd()

        self.gl.glPopMatrix()

    def crearConoWireFrame(self,r,g,b,s,radio,alto,ex,ey,ez,tx,ty,tz,rx,ry,rz):

        self.gl.glPushMatrix()

        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glColor3f(r,g,b)
        self.gl.glLineWidth(s)

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        for i in self.np.arange(0, 7, 0.1):
            auxX = math.sin(i) * radio
            auxZ = math.cos(i) * radio
            self.gl.glVertex(auxX, 0, auxZ)

        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)

        for i in self.np.arange(0, 6.309, 0.1):
            auxX = math.sin(i) * radio
            auxZ = math.cos(i) * radio
            self.gl.glVertex(auxX, 0, auxZ)
            self.gl.glVertex(0, alto, 0)
        self.gl.glEnd()

        self.gl.glPopMatrix()