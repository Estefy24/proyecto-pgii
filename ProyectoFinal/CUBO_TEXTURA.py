from OpenGL.GL import *
from OpenGL.GLU import *
import pygame
from pygame.locals import *



def draw(x,y,z,w,h,p,rx,ry,rz):
    glPushMatrix()
    glTranslatef(x,y,z)
    glRotatef(rx,1,0,0)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rz, 0, 0, 1)
    glScalef(w,h,p)


    # Load the textures
    textura = pygame.image.load("texturas/suelo.jpg")

    # Retrieve the texture data
    texture_data = pygame.image.tostring(textura, 'RGB', True)

    # Generate a texture id
    texture_id = glGenTextures(1)
    # Tell OpenGL we will be using this texture id for texture operations
    glBindTexture(GL_TEXTURE_2D, texture_id)

    # Tell OpenGL how to scale images
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

    # Tell OpenGL that data is aligned to byte boundries
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)

    # Get the dimensions of the image
    width, height = textura.get_rect().size

    gluBuild2DMipmaps(GL_TEXTURE_2D,
                      3,
                      width,
                      height,
                      GL_RGB,
                      GL_UNSIGNED_BYTE,
                      texture_data)







    glBegin(GL_QUADS)
    glTexCoord2f(0, 1)
    glVertex3f(1.000000, 1.000000, -1.000000)
    glTexCoord2f(1, 1)
    glVertex3f(-1.000000, 1.000000, -1.000000)
    glTexCoord2f(1, 0)
    glVertex3f(-1.000000, 1.000000, 1.000000)
    glTexCoord2f(0, 0)
    glVertex3f(1.000000, 1.000000, 1.000000)
    glEnd()

    glBegin(GL_QUADS)
    glTexCoord2f(0, 1)
    glVertex3f(1.000000, -1.000000, 1.000000)
    glTexCoord2f(1, 1)
    glVertex3f(1.000000, 1.000000, 1.000000)
    glTexCoord2f(1, 0)
    glVertex3f(-1.000000, 1.000000, 1.000000)
    glTexCoord2f(0, 0)
    glVertex3f(-1.000000, -1.000000, 1.000000)
    glEnd()

    glBegin(GL_QUADS)
    glTexCoord2f(0, 1)
    glVertex3f(-1.000000, -1.000000, 1.000000)
    glTexCoord2f(1, 1)
    glVertex3f(-1.000000, 1.000000, 1.000000)
    glTexCoord2f(1, 0)
    glVertex3f(-1.000000, 1.000000, -1.000000)
    glTexCoord2f(0, 0)
    glVertex3f(-1.000000, -1.000000, -1.000000)
    glEnd()

    glBegin(GL_QUADS)
    glTexCoord2f(0, 1)
    glVertex3f(-1.000000, -1.000000, -1.000000)
    glTexCoord2f(1, 1)
    glVertex3f(1.000000, -1.000000, -1.000000)
    glTexCoord2f(1, 0)
    glVertex3f(1.000000, -1.000000, 1.000000)
    glTexCoord2f(0, 0)
    glVertex3f(-1.000000, -1.000000, 1.000000)
    glEnd()

    glBegin(GL_QUADS)
    glTexCoord2f(0, 1)
    glVertex3f(1.000000, -1.000000, -1.000000)
    glTexCoord2f(1, 1)
    glVertex3f(1.000000, 1.000000, -1.000000)
    glTexCoord2f(1, 0)
    glVertex3f(1.000000, 1.000000, 1.000000)
    glTexCoord2f(0, 0)
    glVertex3f(1.000000, -1.000000, 1.000000)
    glEnd()

    glBegin(GL_QUADS)
    glTexCoord2f(0, 1)
    glVertex3f(-1.000000, -1.000000, -1.000000)
    glTexCoord2f(1, 1)
    glVertex3f(-1.000000, 1.000000, -1.000000)
    glTexCoord2f(1, 0)
    glVertex3f(1.000000, 1.000000, -1.000000)
    glTexCoord2f(0, 0)
    glVertex3f(1.000000, -1.000000, -1.000000)
    glEnd()
    glPopMatrix()

